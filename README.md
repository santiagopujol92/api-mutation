# Introduction:

NodeJS Api Test Project v16.13.2
using `Express`, `Firebase`, `Jest`, `TypeScript`

# Local Usage:
* install dependencies with `npm install`
* start server with `npm run dev`
* to start the api tests you must run `npm test`

# Endpoints:
### `"{$serverAdress}/mutation/hasMutation"`:
* Method: POST
* Params: dna: String[]
* Result: JSON Object: `{
message: true or false
 }`
* Description:
Method that performs horizontal, vertical and diagonal verification of a mutation of a 6x6 character DNA matrix (example `['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']`). This method return true if have mutation and false if not.

### `"{$serverAdress}/mutation/stats"`
* Method: GET
* Params: none
* Result: JSON Object Array `{[{
    count_mutation: 1,
    count_no_mutation: 1,
    ratio: 50
}]}`
* Description: Obtains the statistics of the mutations resuslts carried out stored in firebase
