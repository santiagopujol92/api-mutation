"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_1 = require("../services/firebase");
const mutationTypes_1 = require("../types/mutationTypes");
// Get horizontal, vertical and crossed mutation of dna Array.
// url Request: /mutation/hasMutation
// params: dna: String[].
// Ex dna param: ['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']
const hasMutation = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    let dna = (_a = req.body.dna) !== null && _a !== void 0 ? _a : null;
    const dnaArr = [];
    // Mapping
    for (let index = 0; index < dna.length; index++) {
        dnaArr[index] = dna[index];
    }
    // Validate input data format
    if (dnaArr.length == 0
        || (dnaArr.length > 0
            && (dnaArr.length != 6 || dnaArr.find(el => el.length != 6) != undefined))) {
        return res.status(200).json({
            message: "Error of mutation data structure . Must be an array of 6 objects with 6 of length. Ex: ['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']"
        });
    }
    // Function to get mutation matchs and stats of String[] array 'dataDna' param.
    // Ex: ['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']
    // Return Boolean result. Ex: true
    const getMutationsDataArr = (dataDna) => {
        const mutations = dataDna.map(element => {
            return checkAndGetMutationByElement(String(element));
        });
        const count_mutation = mutations.filter(element => element.mutation).length;
        const count_no_mutation = mutations.filter(element => !element.mutation).length;
        const statsMutations = {
            count_mutation: count_mutation,
            count_no_mutation: count_no_mutation,
            ratio: Number(((count_mutation * 100) / (count_mutation + count_no_mutation)).toFixed(2))
        };
        return {
            mutations,
            statsMutations
        };
    };
    // Function to check and get the mutation match by String 'element' param. Ex: 'AGAAAA'
    // Return Any Object. Ex: { mutation: true, item: 'AGAAAA' }
    const checkAndGetMutationByElement = (element) => {
        let result = {
            mutation: false,
            item: element
        };
        mutationTypes_1.MutationDataMatch.forEach(charMutation => {
            let secuenceLetterMatch = 0;
            for (let index = 0; index < element.length; index++) {
                const char = element[index];
                if (char === charMutation) {
                    secuenceLetterMatch++;
                }
                else {
                    secuenceLetterMatch = 0;
                }
                if (secuenceLetterMatch == 4) {
                    result.mutation = true;
                    break;
                }
            }
        });
        return result;
    };
    // Function to check Horizontal mutation matchs by String[] array 'dataDna' param.
    // Ex: ['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']
    // Return Boolean result. Ex: true
    const hasHorizontalMutation = (dataDna) => {
        let dataResultMutations = getMutationsDataArr(dataDna);
        let resultGlobalMutation = dataResultMutations.mutations.find(e => e.mutation);
        console.log("Horizontal", dataResultMutations);
        return {
            mutation: resultGlobalMutation ? resultGlobalMutation.mutation : false,
            statsMutation: dataResultMutations.statsMutations
        };
    };
    // Function to check Vertical mutation matchs by String[] array 'dataDna' param.
    // Ex: ['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']
    // Return Boolean result. Ex: true
    const hasVerticalMutation = (dataDna) => {
        let newVerticalElement = '';
        let newArrayHorizontalElements = [];
        for (let charIndex = 0; charIndex < dataDna[0].length; charIndex++) {
            newVerticalElement = '';
            dataDna.forEach((element) => {
                newVerticalElement = newVerticalElement + element[charIndex];
            });
            newArrayHorizontalElements.push(newVerticalElement);
        }
        let dataResultMutations = getMutationsDataArr(newArrayHorizontalElements);
        let resultGlobalMutation = dataResultMutations.mutations.find(e => e.mutation);
        console.log("Vertical", dataResultMutations);
        return {
            mutation: resultGlobalMutation ? resultGlobalMutation.mutation : false,
            statsMutation: dataResultMutations.statsMutations
        };
    };
    // Function to check crossed mutation matchs by String[] array 'dataDna' param.
    // Ex: ['AATTCC', 'AAGGTT', 'AATTTT', 'AAGGTT', 'AAGGTT', 'AATTCC']
    // Return Boolean result. Ex: true
    const hasCrossedMutation = (dataDna) => {
        let newCrossedElement = '';
        let newArrayCrossedElements = [];
        // Search diagonal left mutation to bottom right
        for (let i = 0; i < dataDna.length; i++) {
            for (let j = 0; j < dataDna[i].length; j++) {
                if (i === j) {
                    newCrossedElement = newCrossedElement + dataDna[i][j];
                }
                ;
            }
            ;
        }
        ;
        newArrayCrossedElements.push(newCrossedElement);
        // Search diagonal right mutation to bottom left
        newCrossedElement = '';
        let charIndex = dataDna[0].length - 1;
        for (let i = 0; i < dataDna.length; i++) {
            if (charIndex >= 0) {
                newCrossedElement = newCrossedElement + dataDna[i][charIndex];
            }
            charIndex--;
        }
        ;
        newArrayCrossedElements.push(newCrossedElement);
        let dataResultMutations = getMutationsDataArr(newArrayCrossedElements);
        let resultGlobalMutation = dataResultMutations.mutations.find(e => e.mutation);
        console.log("Diagonal", dataResultMutations);
        return {
            mutation: resultGlobalMutation ? resultGlobalMutation.mutation : false,
            statsMutation: dataResultMutations.statsMutations
        };
    };
    // Save Mutation to Firebase
    const saveMutationFirebaseDB = (statMutation) => {
        (0, firebase_1.addDocByRef)("stats_mutations", statMutation);
    };
    // Function to get the results of each method and look in console
    const execCheckMutation = () => {
        const resHM = hasHorizontalMutation(dnaArr);
        const resVM = hasVerticalMutation(dnaArr);
        const resCM = hasCrossedMutation(dnaArr);
        let count_mutation = (resHM.statsMutation.count_mutation + resVM.statsMutation.count_mutation + resCM.statsMutation.count_mutation);
        let count_no_mutation = (resHM.statsMutation.count_no_mutation + resVM.statsMutation.count_no_mutation + resCM.statsMutation.count_no_mutation);
        let ratio = Number(((count_mutation * 100) / (count_mutation + count_no_mutation)).toFixed(2));
        const statMutation = {
            count_mutation: count_mutation,
            count_no_mutation: count_no_mutation,
            ratio: ratio // Mutation Probability
        };
        console.log('Final Result', statMutation);
        saveMutationFirebaseDB(statMutation);
        // // If one case make a match mutation the api response TRUE
        return resHM.mutation || resVM.mutation || resCM.mutation;
    };
    if (execCheckMutation()) {
        return res.status(200).json({
            message: true
        });
    }
    else {
        res.status(403).json({
            message: false
        });
    }
});
// Get data stats of the latest mutations
// url Request: /mutation/stats
// params: none.
const stats = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield (0, firebase_1.getDocsByRef)("stats_mutations");
    return res.status(200).json({
        message: data
    });
});
// Get data stats of the latest mutations
// url Request: /mutation/
// params: none.
const index = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    return res.status(200).send("Welcome to Mutation Api");
});
exports.default = {
    hasMutation,
    stats,
    index
};
