const axios = require("axios")

const BASE_URL = 'http://localhost:6060';

// Test EndPoint GET /mutation/stats
describe("GET /mutation/stats", () => {
  test("Should respond with 200 status code", async () => {
    const response = await axios.get(`${BASE_URL}/mutation/stats`);
    expect(response.status).toEqual(200)
  });

  test("Should respond be an array", async () => {
    const response = await axios.get(`${BASE_URL}/mutation/stats`);
    expect(response.data.message).toBeInstanceOf(Array)
  });

  test("Should have a content-type: application/json", async () => {
    const response = await axios.get(`${BASE_URL}/mutation/stats`);
    expect(response.headers["content-type"]).toEqual(
      expect.stringContaining("json")
    )
  });
});

// Test EndPoint POST /mutation/hastMutation
describe("POST /mutation/hasMutation", () => {
  test("Should respond with 200 status code when mutation is true", async () => {
    const response = await axios.post(`${BASE_URL}/mutation/hasMutation`, {
      dna: ['AATTCC', 'AAGGTT', 'AATTCC', 'AAGGTT', 'AAGGTT', 'AATTCC']
    })
    expect(response.status).toEqual(200)
  });

  test("Should respond with 403 status code when mutation is false", async () => {
    try {
      const response = await axios.post(`${BASE_URL}/mutation/hasMutation`, {
        dna: ['ABCDEF', 'GHIJKL', 'MNOPQR', 'TUVWXY', 'ZPERTY', 'ATTRCC']
      })
    } catch (error) {
      expect(error.response.status).toEqual(403)
    }
  });

  test("Should respond error validation Must be an array of 6 objects with 6 of length", async () => {
    const response = await axios.post(`${BASE_URL}/mutation/hasMutation`, {
      dna: ['ABCDEF', 'GHIJKL', 'MNPQR', 'TUVWXY', 'ZPERTY', 'ATTRC']
    })
    expect(response.data.message).toEqual(
      expect.stringContaining("Error of mutation data structure")
    )
  });

  test("Should respond error validation Must be an array of 6 objects", async () => {
    const response = await axios.post(`${BASE_URL}/mutation/hasMutation`, {
      dna: ['ABCDEF', 'GHIJKL', 'MNPQR', 'TUVWXY', 'ZPERTY']
    })
    expect(response.data.message).toEqual(
      expect.stringContaining("Error of mutation data structure")
    )
  });

});
