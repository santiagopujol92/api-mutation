interface StatMutation {
    count_mutation: Number,
    count_no_mutation: Number,
    ratio: Number
}

const MutationDataMatch: String[] = [
    "A", "T", "C", "G"
]

export {
    StatMutation,
    MutationDataMatch
}