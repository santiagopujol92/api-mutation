/** src/routes/mutation.ts */
import express from 'express';
import controller from '../controllers/mutation';
const router = express.Router();

router.post('/hasMutation', controller.hasMutation);
router.get('/stats', controller.stats);
router.get('/', controller.index);

export default router;
