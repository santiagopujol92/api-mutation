/** src/services/firebase.ts */
import {
    initializeApp
} from "firebase/app";
import {
    getFirestore,
    collection,
    getDocs,
    addDoc,
}
from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCHmzCwGCo_4u50kj93WWdB4N-9c1MIgYU",
    authDomain: "prueba-api-f8fb7.firebaseapp.com",
    projectId: "prueba-api-f8fb7",
    storageBucket: "prueba-api-f8fb7.appspot.com",
    messagingSenderId: "545151736765",
    appId: "1:545151736765:web:19228fbf5f4572137250d4"
};

initializeApp(firebaseConfig);
const db = getFirestore();

// Add doc by ref to firebase
async function addDocByRef(colRef: string, data: any) {
    try {
        await addDoc(collection(db, colRef), data)
    } catch (error) {
        console.log('error adding ref', error);
    }
}

// Get all docs by ref to firebase
async function getDocsByRef(colRef: string) {
    const col = collection(db, colRef)
    const snapshot = await getDocs(col);
    return snapshot.docs.map(doc => doc.data());
}

export {
    addDocByRef,
    getDocsByRef
};
